<#include "site.inc.ftl">
<#assign creationDate = content.creation_date?date("yyyy-MM-dd")>
<@page model=content section="articles">
    <#compress>
    <main class="section">
        <div class="container content">
            <h1 class="title is-1">${content.title}</h1>
            <p class="has-text-grey">
                <@h.time datetime=creationDate?date>${creationDate?date?string.long}</@h.time>
                <#if (content.date?date != creationDate?date)>
                    <@h.time datetime=content.date?date>${content.date?date?string.long}</@h.time>
                </#if>
            </p>
            <@tagP content.tags! />
            </#compress>
            ${content.body}
            <#compress>
        </div>
    </main>
    </#compress>
</@page>
