<#--
  - JBake FreeMarker library
  - Author: Laurent Pireyn
  -->

<#function canonical uri>
    <#return config.site_host + "/" + uri>
</#function>

<#function contentByType type>
    <#return all_content?filter(it -> it.type == type)>
</#function>

<#function version>
    <#-- Remove the leading 'v' -->
    <#return .globals.version[1..]>
</#function>

<#function postByName name>
    <#list published_posts as post>
        <#if (post.name! == name)>
            <#return post>
        </#if>
    </#list>
    <#return {}>
</#function>

<#function relPath path>
    <#return content.rootpath + path>
</#function>

<#function tagByName name>
    <#list tags as tag>
        <#if (tag.name == name)>
            <#return tag>
        </#if>
    </#list>
    <#return {}>
</#function>
