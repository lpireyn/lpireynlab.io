<#ftl output_format="XML">
<#compress>

<#import "jbake.lib.ftl" as jbake>

<#-- TODO: Extract this to a dedicated FreeMarker library -->
<#function rssDateTime dateTime>
    <#return dateTime?string("EEE, d MMM yyyy HH:mm:ss Z")>
</#function>

<?xml version="1.0" encoding="UTF-8"?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel>
        <title>${config.site_title}</title>
        <link>${config.site_host}</link>
        <atom:link rel="self" href="${config.site_host}/${config.feed_file}" type="application/rss+xml" />
        <language>en</language>
        <generator>JBake ${jbake.version()}</generator>
        <pubDate>${rssDateTime(published_date)}</pubDate>
        <lastBuildDate>${rssDateTime(published_date)}</lastBuildDate>
        <#list published_content as content>
            <#if (content.type != "article" && content.type != "post")>
                <#continue>
            </#if>
            <item>
                <guid>${jbake.canonical(content.uri)}</guid>
                <title>${content.title}</title>
                <link>${jbake.canonical(content.uri)}</link>
                <description>${content.description!""}</description>
                <#list content.tags! as tag>
                    <category>${tag}</category>
                </#list>
                <pubDate>${rssDateTime(content.date)}</pubDate>
            </item>
        </#list>
    </channel>
</rss>

</#compress>
