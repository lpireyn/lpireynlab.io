<#include "site.inc.ftl">
<@page model=content section="articles">
    <#compress>
    <main class="section">
        <div class="container content">
            <h1 class="title is-1">${content.title}</h1>
            <#list jbake.contentByType("article")?sort_by("title") as article>
                <article class="box">
                    <h2 class="title is-3">
                        <a href="${jbake.relPath(article.uri)}">${article.title}</a>
                    </h2>
                    <#if (article.description?has_content)>
                        <p>
                            ${article.description}
                        </p>
                    </#if>
                    <@tagP article.tags! />
                </article>
            </#list>
        </div>
    </main>
    </#compress>
</@page>
