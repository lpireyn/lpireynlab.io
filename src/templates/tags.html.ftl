<#include "site.inc.ftl">
<@page model={
    "title": "Tags",
    "uri": "tags"
} section="tags">
    <#compress>
    <main class="section">
        <div class="container content">
            <h1 class="title is-1">Tags</h1>
            <p>
                <#list tags?sort_by("name") as tagModel>
                    <a href="${jbake.relPath(tagModel.uri)}" class="button">${tagModel.name}</a>
                    <#sep> </#sep>
                </#list>
            </p>
        </div>
    </main>
    </#compress>
</@page>
