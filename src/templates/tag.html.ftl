<#include "site.inc.ftl">
<#assign tagModel = jbake.tagByName(tag)>
<@page model={
    "title": "Tag: " + tagModel.name,
    "uri": tagModel.uri
} section="tags">
    <#compress>
    <main class="section">
        <div class="container content">
            <h1 class="title is-1">Tag: ${tagModel.name}</h1>
            <#list tagModel.tagged_documents?filter(it -> it.type == "article")?sort_by("title")>
                <section>
                    <h2 class="subtitle is-3">Articles tagged with ${tagModel.name}</h2>
                    <#items as article>
                        <article class="box">
                            <h3 class="title is-5">
                                <a href="${jbake.relPath(article.uri)}">${article.title}</a>
                            </h3>
                            <#if (article.description?has_content)>
                                <p>
                                    ${article.description}
                                </p>
                            </#if>
                            <@tagP article.tags! />
                        </article>
                    </#items>
                </section>
            </#list>
            <#list tagModel.tagged_posts>
                <section>
                    <h2 class="subtitle is-3">Posts tagged with ${tagModel.name}</h2>
                    <#items as post>
                        <article class="box">
                            <h2 class="title is-5">
                                <a href="${jbake.relPath(post.uri)}">${post.title}</a>
                            </h2>
                            <p class="has-text-grey">
                                <@h.time datetime=post.date?date>${post.date?date?string.long}</@h.time>
                            </p>
                            <#if (post.description?has_content)>
                                <p>
                                    ${post.description}
                                </p>
                            </#if>
                            <@tagP post.tags! />
                        </article>
                    </#items>
                </section>
            </#list>
        </div>
    </main>
    </#compress>
</@page>
