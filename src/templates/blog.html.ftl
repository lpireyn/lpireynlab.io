<#include "site.inc.ftl">
<@page model=content section="blog">
    <#compress>
    <main class="section">
        <div class="container content">
            <h1 class="title is-1">${content.title}</h1>
            <#list published_posts as post>
                <article class="box">
                    <h2 class="title is-3">
                        <a href="${jbake.relPath(post.uri)}">${post.title}</a>
                    </h2>
                    <p class="has-text-grey">
                        <@h.time datetime=post.date?date>${post.date?date?string.long}</@h.time>
                    </p>
                    <#if (post.description?has_content)>
                        <p>
                            ${post.description}
                        </p>
                    </#if>
                    <@tagP post.tags! />
                </article>
            </#list>
        </div>
    </main>
    </#compress>
</@page>
