<#--
  - cdnjs.js FreeMarker library
  - Author: Laurent Pireyn
  -->

<#function cdnjsUri path>
    <#return "https://cdnjs.cloudflare.com/ajax/libs/" + path>
</#function>
