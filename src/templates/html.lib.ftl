<#--
  - HTML FreeMarker library
  - Author: Laurent Pireyn
  -->

<#macro attr name value skipEmpty=true>
    <#t><#if (!skipEmpty || value?has_content)> ${name}="${value}"</#if>
</#macro>

<#macro attrs hash skipEmpty=true>
    <#t><#list hash?keys as name><@attr name=name value=hash[name] skipEmpty=skipEmpty /></#list>
</#macro>

<#macro head title charset=config.render_encoding!"" metaHash={} links=[]>
    <head>
        <#if (charset?has_content)>
            <meta charset="${charset}" />
        </#if>
        <title>${title}</title>
        <#list metaHash?keys as name>
            <@meta name=name content=metaHash[name] />
        </#list>
        <#list links as pair>
            <@link rel=pair[0] href=pair[1] />
        </#list>
        <#nested>
    </head>
</#macro>

<#macro html lang="">
    <!DOCTYPE html><#lt>
    <html<@attr name="lang" value=lang />><#lt>
        <#nested>
    </html><#lt>
</#macro>

<#macro link rel href>
    <#if (href?has_content)><link rel="${rel}" href="${href}"></#if><#lt>
</#macro>

<#macro meta name content>
    <#if (content?has_content)><meta name="${name}" content="${content}"></#if><#lt>
</#macro>

<#macro script src>
    <script src="${src}"></script>
</#macro>

<#macro time datetime>
    <time datetime="${datetime?datetime_if_unknown?string.iso}"><#nested></time><#t>
</#macro>
