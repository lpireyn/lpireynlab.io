<#include "site.inc.ftl">
<@page model=content section="blog">
    <#compress>
    <main class="section">
        <div class="container content">
            <h1 class="title is-1">${content.title}</h1>
            <p class="has-text-grey">
                <@h.time datetime=content.date?date>${content.date?date?string.long}</@h.time>
            </p>
            <@tagP content.tags! />
            </#compress>
            ${content.body}
            <#compress>
        </div>
    </main>
    </#compress>
</@page>
