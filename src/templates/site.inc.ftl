<#ftl output_format="HTML">

<#--
  - Site-specific FreeMarker include file
  - Author: Laurent Pireyn
  -->

<#import "cdnjs.lib.ftl" as cdnjs>
<#import "html.lib.ftl" as h>
<#import "jbake.lib.ftl" as jbake>

<#macro body model section="">
    <#compress>
    <body class="line-numbers">
        <nav class="navbar is-dark" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
                <a href="${jbake.relPath("")}" class="navbar-item is-size-4 has-text-weight-semibold">Laurent Pireyn</a>
                <a class="navbar-burger" role="button" aria-label="menu" aria-expanded="false" data-target="navbarMenu">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>
            <div id="navbarMenu" class="navbar-menu">
                <div class="navbar-start">
                    <@navbarItem uri=jbake.relPath("articles") active=(section == "articles")>Articles</@navbarItem>
                    <@navbarItem uri=jbake.relPath("blog") active=(section == "blog")>Blog</@navbarItem>
                    <@navbarItem uri=jbake.relPath("tags") active=(section == "tags")>Tags</@navbarItem>
                    <@navbarItem uri="https://www.hackerrank.com/lpireyn">HackerRank</@navbarItem>
                    <@navbarItem uri=jbake.relPath(config.feed_file)>RSS</@navbarItem>
                </div>
            </div>
        </nav>
        </#compress>
        <#nested>
        <#compress>
        <footer class="footer has-background-grey-lighter has-text-grey has-text-centered">
            <p>
                Copyright &copy; 2022 Laurent Pireyn
            </p>
            <p>
                Made with <a href="https://jbake.org/">JBake ${jbake.version()}</a>,
                <a href="https://bulma.io/">Bulma ${config.bulma_version}</a>,
                and <a href="https://prismjs.com/">Prism ${config.prism_version}</a>.
            </p>
            <p class="is-size-7">
                Generated on <@h.time datetime=published_date?datetime>${published_date?datetime?iso_utc}</@h.time>
            </p>
        </footer>
        <@h.script src=cdnjs.cdnjsUri("prism/${config.prism_version}/components/prism-core.min.js") />
        <@h.script src=cdnjs.cdnjsUri("prism/${config.prism_version}/plugins/autoloader/prism-autoloader.min.js") />
        <@h.script src=cdnjs.cdnjsUri("prism/${config.prism_version}/plugins/line-numbers/prism-line-numbers.min.js") />
    </body>
    </#compress>
</#macro>

<#macro head model>
    <#local title = config.site_title>
    <#if (model.title?has_content)>
         <#local title = title + " | " + model.title>
    </#if>
    <#compress>
    <@h.head
        title=title
        metaHash={
            "viewport": "width=device-width, initial-scale=1",
            "description": model.description!"",
            "author": model.author!config.site_author!"",
            "generator": "JBake " + jbake.version()
        }
        links=[
            ["canonical", jbake.canonical(model.uri)],
            ["stylesheet", cdnjs.cdnjsUri("bulma/${config.bulma_version}/css/bulma.min.css")],
            ["stylesheet", cdnjs.cdnjsUri("prism/${config.prism_version}/plugins/line-numbers/prism-line-numbers.min.css")],
            ["stylesheet", cdnjs.cdnjsUri("prism-themes/${config.prismthemes_version}/prism-${config.prism_theme}.min.css")],
            ["stylesheet", jbake.relPath("css/site.css")]
        ]
    >
        <#-- TODO: Improve link and links macros in HTML library, so the type can be specified -->
        <link rel="alternate" type="application/rss+xml" href="${jbake.relPath(config.feed_file)}">
        <#nested>
    </@h.head>
    </#compress>
</#macro>

<#macro html model>
    <@h.html lang=model.lang!"">
        <#nested>
    </@h.html>
</#macro>

<#macro navbarItem uri active=false>
    <a href="${uri}" class="navbar-item<#if (active)> is-active</#if>"><#nested></a>
</#macro>

<#macro page model section="">
    <@html model=model>
        <@head model=model />
        <@body model=model section=section>
            <#nested>
        </@body>
    </@html>
</#macro>

<#macro tagLink tagObject>
    <a href="${jbake.relPath(tagObject.uri)}" class="tag">${tagObject.name}</a><#t>
</#macro>

<#macro tagP tagNames>
    <#if (tagNames!?has_content)>
        <p>
            <#list tagNames!?sort as tagName>
                <@tagLink jbake.tagByName(tagName) /><#sep>&nbsp;</#sep><#t>
            </#list>
        </p>
    </#if>
</#macro>
