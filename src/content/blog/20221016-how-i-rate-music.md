type=post
title=How I rate music
description=A short explanation of how I rate music using a 5-star scale.
creation_date=2022-10-16
date=2022-10-16
lang=en
tags=Music
~~~~~~
The [ID3 v2.3.0 specification](https://id3.org/id3v2.3.0) defines the *popularimeter* frame.
It allows a user (identified by their email address) to store their rating of a track.
The rating itself is a byte, so it can easily represent values on any sensible scale.

A lot of music players and collection managers allow the user to define the number of values of that scale -
often represented as *stars*.
This is the case of [Quod Libet](https://quodlibet.readthedocs.io/en/latest/), one of my personal favorites.

I use a 5-star scale.
I want tracks to have no stars by default, so I know I haven't rated them yet.
When I rate a track, I give it from 1 to 5 stars based on how I like it and how likely it is I'll listen to it again:

| Rating | How I like the track | How likely it is I'll listen to it again |
|---|---|---|
| &#x2605; | It's terrible | I never want to hear it again |
| &#x2605;&#x2605; | It's OK | I don't mind hearing it |
| &#x2605;&#x2605;&#x2605; | It's good | I'll listen to it again sometimes |
| &#x2605;&#x2605;&#x2605;&#x2605; | It's fantastic | I'll definitely listen to it again |
| &#x2605;&#x2605;&#x2605;&#x2605;&#x2605; | It's a masterpiece | I'll certainly listen to it again when I want to be blown away |

Ratings can be uploaded to [MusicBrainz](https://musicbrainz.org/).
