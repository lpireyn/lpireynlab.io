type=post
title=Parameter vs. argument
description=A short note about the difference between parameter and argument in programming languages.
creation_date=2022-10-11
date=2022-10-11
lang=en
tags=English,Programming
~~~~~~
To paraphrase [Wikipedia](https://en.wikipedia.org/wiki/Parameter_(computer_programming)):

> *Parameters* are the variables defined in a function to refer to the *arguments* actually provided when it is invoked.

```java
// n is the parameter of the sqr method
int sqr(int n) {
    return n * n;
}
```

```java
// x is the argument to the sqr method
sqr(x);
```
