type=article
title=JBake
description=Advanced tips for JBake.
creation_date=2022-09-10
date=2022-09-10
lang=en
tags=JBake
~~~~~~
## Overview

[JBake](https://jbake.org/) is a static website generator written in Java.

Templates can be written in the following languages:

- [FreeMarker](https://freemarker.apache.org/)
- Thymeleaf
- Jade
- Pebble
- Groovy SimpleTemplate
- Groovy XmlTemplate
- Groovy MarkupTemplate

Contents can be written in the following formats:

- Markdown
- AsciiDoc
- HTML

## Tips

### A more flexible homepage

JBake generates a homepage that uses the `index` template.
If you'd rather use a content file for the homepage (e.g. to use a markup language other than HTML),
you can follow the following instructions:

1. Disable the default homepage generation by adding `render.index=false` to the `jbake.properties` file.
    You can also delete the `index.html` template to keep things tidy.
2. Create a template for the homepage (e.g. `home.html.ftl`) in the `templates` directory.
    Define this template in the `jbake.properties` file (e.g. `template.home.file = home.html.ftl`).
    This template will receive a `content` model and can therefore make use of `content.body`.
3. Create an content file named `index` in the `content` directory (e.g. `index.md`).
    In its *front matter*, set its type to the name of the template (e.g. `type=home`).

JBake will now process the new template with the content file.
As the file generated is named `index.html`, it will be used as homepage.
