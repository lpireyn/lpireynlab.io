plugins {
    id("org.jbake.site") version "5.5.0"
}

jbake {
    version = "2.6.7"
    srcDirName = "src"
}

tasks.assemble {
    dependsOn(tasks.bake)
}
