# Laurent Pireyn

This is my [personal website](https://laurent.pireyn.be/).

## Building

- [JBake](https://jbake.org)
- [Gradle](https://gradle.org)

## Hosting

- [GitLab](https://gitlab.com/lpireyn/lpireyn.gitlab.io)

## License

This project is licensed under the [Creative Commons Attribution Share Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
